from MugshotToPlanche import MugshotToPlanche
from StatsFileInDir import StatsFileInDir


print("Generation des petits index sur papier A4")
m = MugshotToPlanche('/home/stef/dev/Mugshot/travail/phase1',  '/home/stef/dev/Mugshot/travail/phase3/smallIndexA4')
m.setTailleVignette((151, 151))
m.setTaillePapiercm((21, 29.7))
m.setGeometrie((16,16))
m.createPlanches()


print("Generation des petits index All In One")
m = MugshotToPlanche('/home/stef/dev/Mugshot/travail/phase1',  '/home/stef/dev/Mugshot/travail/phase3/AllInOne')
m.setTailleVignette((151, 151))
m.setTaillePapiercm((26, 35))
m.setGeometrie((19,20))
m.createPlanches()


print("Generation des petits index")
m = MugshotToPlanche('/home/stef/dev/Mugshot/travail/phase1',  '/home/stef/dev/Mugshot/travail/phase3/smallIndex')
m.setTailleVignette((151, 151))
m.setGeometrie((8,7))
m.createPlanches()

print("Generation des index")
m = MugshotToPlanche('/home/stef/dev/Mugshot/travail/phase1',  '/home/stef/dev/Mugshot/travail/phase3/index')
m.setTailleVignette((215, 215))
m.setGeometrie((5,5))
m.createPlanches()

print("Generation des Mugshots")
m = MugshotToPlanche('/home/stef/dev/Mugshot/travail/phase1',  '/home/stef/dev/Mugshot/travail/phase3/mugshot')
m.createPlanches()

print("Generation des Mugshots cavaliers")
m = MugshotToPlanche('/home/stef/dev/Mugshot/travail/phase1',  '/home/stef/dev/Mugshot/travail/phase3/mugshotCavalier')
m.activeModeCavalier()
m.setGeometrie((1,2))
m.createPlanches()



print("Stats")
s = StatsFileInDir('/home/stef/dev/Mugshot/travail/phase1/')
