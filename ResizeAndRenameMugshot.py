import os
from PIL import Image

# Source dir au format :
#  Collection/Genre/CategorieAge
class ResizeAndRenameMugshot:
    def __init__(self,  srcDir,  dstDir):
        self._serie = ""
        self._genre = ""
        self._catAge = ""
        self._compteur = 1
        self.srcDir = srcDir
        self.dstDir = dstDir
        
        self.width = 520
        self.height = 520

    def resizeAndRename(self):
        self.walkThePath( self.srcDir,  0)

    def traiteFichier(self,  path,  file ):
        srcFile = os.path.join(path,  file)
        dstFile = os.path.join(path.replace(self.srcDir,  self.dstDir),  self.getPlancheName())
        #print("copie en {} de {}".format( dstFile,  srcFile) )
        imSrc = Image.open(srcFile).resize((self.width,  self.height))
        
        imSrc.save(dstFile)
        self._compteur += 1
        
    def getPlancheName(self):
        return "{0}{1}{2}{3:03d}.png".format(self._serie,  self._genre,  self._catAge, self._compteur)
        
    def dirCreate(self, dir):
        try :
            os.makedirs( dir )
        except OSError :
            print("Repertoire deja existant")
    def walkThePath(self,  path,  depth):
        if ( depth == 0 ):
            self.dirCreate(os.path.join(self.dstDir))
        elif ( depth == 1):
            self._serie = os.path.basename( path )
            self._compteur = 1
            self.dirCreate(os.path.join(self.dstDir, self._serie) )
            print("serie spotted : " + self._serie)
        elif ( depth == 2):
            self._genre = os.path.basename( path )
            self._compteur = 1
            self.dirCreate(os.path.join(self.dstDir, self._serie, self._genre) )
        elif ( depth == 3):
            self._catAge = os.path.basename( path )
            self._compteur = 1
            self.dirCreate(os.path.join(self.dstDir, self._serie, self._genre, self._catAge) )
        for file in sorted(os.listdir(path)):
            if ( os.path.isdir( os.path.join(path,  file) ) ):
                self.walkThePath( os.path.join(path,  file),  depth+1 )
            else :
                self.traiteFichier( path,  file)
