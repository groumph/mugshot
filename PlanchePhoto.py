from PIL import Image,  ImageFont,  ImageDraw
#import os

class PlanchePhoto :
    def __init__( self,  nbLignes,  nbColonnes,  width,  height,  listeFichiers,  fileOut):
        self.nbLignes = nbLignes
        self.nbColonnes = nbColonnes
        self.width = width
        self.height = height
        self.listeFichiers = listeFichiers
        self.fileOut = fileOut
        
        self.widthSrcFile = 520
        self.heightSrcFile = 520
        
        self.tailleFont = 30
        self.font = ImageFont.truetype('FreeMono.ttf',  self.tailleFont )
        self.margeTexteX = 5 

        self._modeCavalier = False
        self._taillePliure = 10
        self.fond = (255, 255, 255, 0)
        self.bordure = (0, 0, 0, 0)
        self.ecriture = (0, 0, 0, 0)
        self.initSize()

    def setModeCavalier(self, mode):
        self._modeCavalier = mode

    def initSize(self):
        self.widthCartouche = self.widthSrcFile
        self.heightCartouche = int(self.heightSrcFile * 0.43)
        self.widthBox = self.widthSrcFile
        self.heightBox = int(self.heightSrcFile * 1.43)
        self.margeTexteY = self.heightCartouche - self.tailleFont
        
    def setWidthSrcFile(self,  width):
        self.widthSrcFile = width
        self.initSize()

    def setHeightSrcFile(self,  height):
        self.heightSrcFile = height
        self.initSize()
        

    def genPetiteImage(self, fileIn, texte):
        petiteImage = Image.new('RGB', (self.widthBox, self.heightBox), self.fond)
        draw = ImageDraw.Draw(petiteImage)
        srcFile = fileIn
        srcImage = Image.open(srcFile).resize((self.widthSrcFile, self.heightSrcFile ), Image.ANTIALIAS)
        # Collage de l'image
        petiteImage.paste(srcImage,  (0,  0))
        xTexte = self.margeTexteX
        yTexte = self.widthSrcFile + self.margeTexteY
        draw.text( (xTexte,  yTexte),  texte,  font=self.font,  fill=self.ecriture )
        # Dessin du cadre
        draw.rectangle( [ 0, 0,  self.widthBox-1, self.heightBox-1], outline=self.bordure )
        return petiteImage

    def createPlanche(self):
        image = Image.new('RGB', (self.width, self.height), self.fond)
        i = 0
        for l in range(self.nbLignes):
            for c in range(self.nbColonnes):
                # Emplacement de l'image à coller
                #xImage = c * self.widthSrcFile
                #yImage = l * ( self.heightSrcFile + self.heightCartouche )
                # Image à coller
                try :
                    srcFile = self.listeFichiers[i]
                    #texte = os.path.splitext(os.path.basename(srcFile) ) [0]
                    texte = srcFile.getId()
                    #petiteImage = self.genPetiteImage(srcFile, texte)
                    petiteImage = self.genPetiteImage(srcFile.getPath(), texte)
                    if self._modeCavalier :

                        image.paste(petiteImage.rotate(180),  (self.widthBox*c,  self.heightBox*2*l))
                        image.paste(petiteImage,  (self.widthBox*c, self.heightBox*2*l +self.heightBox + self._taillePliure))
                    else :
                        image.paste(petiteImage,  (self.widthBox*c,  self.heightBox*l))
                    #grandeImage.paste(petiteImage,  (0,  self.heightBox))
                    i+=1
                except IndexError :
                    break
        image.save(self.fileOut, "png")
