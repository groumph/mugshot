import os
from PlanchePhoto import PlanchePhoto
from Fichier import Fichier

class MugshotToPlanche:
    def __init__(self,  baseDir,  outDir):
        self.baseDir = baseDir
        self._taillePapier= (1181,  1772)
        self._tailleVignette=(520, 520)
        self._serie = ""
        self._genre = ""
        self._catAge = ""
        self._compteurPlanche = 0
        self._compteurFichier = 0
        
        self.outDir = outDir
        
        self.nbLignes = 2
        self.nbColonnes = 2
        self._nbFilePerPlanche = self.nbLignes *  self.nbColonnes
        self._files = []
        self._modeCavalier = False
    
    def setTaillePapiercm(self, taille):
        self._taillePapier = ( int( taille[0] * 300 / 2.54), int( taille[1] * 300 / 2.54) )

    def setTaillePapier(self, taille):
        self._taillePapier = taille

    def activeModeCavalier(self):
        self._modeCavalier = True

    def setTailleVignettecm(self,  taille):
        self._tailleVignette = ( int( taille[0] * 300 / 2.54), int( taille[1] * 300 / 2.54) )

    def setTailleVignette(self,  taille):
        self._tailleVignette = taille
        
    def getFichierId(self):
        return "{0}{1}{2}{3:03d}".format(self._serie,  self._genre,  self._catAge, self._compteurFichier)

    def setGeometrie(self,  nb):
        self.nbLignes = nb[0]
        self.nbColonnes = nb[1]
        self._nbFilePerPlanche = self.nbLignes *  self.nbColonnes
    
    def getPlancheName(self):
        return "{0}{1:03d}.png".format(self._serie, self._compteurPlanche)

    def printPlanche(self):
        p = PlanchePhoto(self.nbLignes,  self.nbColonnes,  self._taillePapier[0],  self._taillePapier[1],  self._files,  os.path.join(self.outDir,  self.getPlancheName()) )
        p.setWidthSrcFile(self._tailleVignette[0])
        p.setHeightSrcFile(self._tailleVignette[1])
        p.setModeCavalier( self._modeCavalier)
        p.createPlanche()

    def traiteFichier(self,  path,  file ):
        self._compteurFichier += 1
        if ( len( self._files) >= self._nbFilePerPlanche ) :
            self.printPlanche()
            self._compteurPlanche += 1
            
            #print("compteurFichier : " + str(self._compteurFichier) )
            self._files = []
            self._files.append(Fichier(os.path.join(path,  file), self.getFichierId()))
        else :
            self._files.append(Fichier(os.path.join(path,  file), self.getFichierId()))

#    def getPlancheName(self):
#        return "{0}{1}{2}{3:03d}.png".format(self._serie,  self._genre,  self._catAge, self._compteurFichier)
    
    def createPlanches(self):
        self.walkThePath(self.baseDir,  0)
        self.printPlanche()

    def dirCreate(self, dir):
        try :
            os.makedirs( dir )
        except OSError :
            print("Repertoire deja existant")  
      
    def walkThePath(self,  path,  depth):
        if ( depth == 0 ):
            self.dirCreate(os.path.join(self.outDir))
        elif ( depth == 1):
            if self._serie != "":
                self.printPlanche()
                self._files = []
                self._compteurPlanche = 0
                #self._compteurFichier = 1
            self._serie = os.path.basename( path )
            #self.dirCreate(os.path.join(self.outDir, self._serie) )
        elif ( depth == 2):
            self._genre = os.path.basename( path )
            #self._compteurFichier = 1
            #self.dirCreate(os.path.join(self.outDir, self._serie, self._genre) )
        elif ( depth == 3):
            self._catAge = os.path.basename( path )
            self._compteurFichier = 0
            #self.dirCreate(os.path.join(self.outDir, self._serie, self._genre, self._catAge) )

        for file in sorted(os.listdir(path)):
            if ( os.path.isdir( os.path.join(path,  file) ) ):
                #self._compteurFichier = 1
                self.walkThePath( os.path.join(path,  file),  depth+1 )
            else :
                self.traiteFichier( path,  file)

