import os


class StatsFileInDir:
    def __init__(self, dir):
        self._serie = ""
        self._genre =  ""
        self._catAge = ""
        self._stats = {}
        self.walkThePath(dir,  0)
        self.printStats()

    def getKey(self):
        return "{0}{1}{2}".format(self._serie,  self._genre,  self._catAge)
        
    def traiteFichier(self,  path,  file):
        clef = self.getKey()
        try :
            self._stats[clef] += 1
            
        except :
            self._stats[clef] = 1
        
    def walkThePath(self,  path,  depth):
        
        if ( depth == 1):
            self._serie = os.path.basename( path )
        elif ( depth == 2):
            self._genre = os.path.basename( path )
        elif ( depth == 3):
            self._catAge = os.path.basename( path )

        for file in sorted(os.listdir(path)):
            if ( os.path.isdir( os.path.join(path,  file) ) ):
                self.walkThePath( os.path.join(path,  file),  depth+1 )
            else :
                self.traiteFichier( path,  file)
                
    def printStats(self):
        for clef in self._stats:
            print("{} : {}".format(clef,  self._stats[clef]))
