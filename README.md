# mugshot
Génération de trombinoscopes de PNJ optimisé pour être imprimé par un labo photo sur du papier de 10x15

les fichiers d'entrée doivent être disposé dans une arborescence comportant trois niveaux, la décomposition utilisée ici est purement personnelle, seule le nombre de sous répertoires importe dans le programme.
 * la série (permet de gérer dans un même répertoire un série contemporaine, une de SF et une médiévale)
 * le genre des portraits  (H ou F)
 * la catégorie d'age du portrait (A à E, très subjectif)

Les fichiers d'origine doivent être au format carré, ils seront convertis en des images de 520x520 pour l'impression sur les Mugshots
Chaque fichier sera renommé en concaténant :
 * la série (ou le niveau 1 de sous répertoires)
 * le genre (ou le niveau 2 de sous répertoires)
 * l'age (ou le niveau 3 de sous répertoires)
 * une numérotation unique dans un sous répertoire


Les fichiers générés sont de 4 types différents : 
 * mugshot : les mugshots, au format 1/2 carte magic pour être rangés dans un classeur ultrapro
 * mugshot cavaliers : les mugshots, destinés à être pliés et entreposés à califourchon sur le sommet de l'écran
 * index : des miniatures des mugshot, pour simplifier le parcours de ceux ci. 5x5 image par photo de 10x15
 * smallIndex : des miniatures (timbre poste), pour simplifier le parcours de ceux ci (pour ceux qui ont une bonne vue). 7x8 images par photo de 10x15


Pour le moment pas de lanceur tout public de dispo. Il faut modifier à la main le script test.py et le lancer à la main avec python (v3 utilisé pour les tests).

Les dépendances de python3 à installer :
* PIL
